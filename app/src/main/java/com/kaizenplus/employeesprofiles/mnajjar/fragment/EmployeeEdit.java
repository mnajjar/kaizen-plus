package com.kaizenplus.employeesprofiles.mnajjar.fragment;

import android.app.DatePickerDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.activity.CommonWidget;
import com.kaizenplus.employeesprofiles.mnajjar.database.EmployeeDAO;
import com.kaizenplus.employeesprofiles.mnajjar.database.OpenHelper;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;
import com.kaizenplus.employeesprofiles.mnajjar.util.DateUtil;
import com.kaizenplus.employeesprofiles.mnajjar.util.ValidationUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class EmployeeEdit extends Fragment {

    private EditText emailEdt;
    private EditText passwordEdt;
    private EditText confPasswordEdt;
    private TextInputLayout emailLayout;
    private TextInputLayout passwordLayout;
    private TextInputLayout confPasswordLayout;
    private EditText nameEdt;
    private EditText positionEdt;
    private EditText birthdayEdt;
    private EditText addressEdt;
    private EditText salaryEdt;
    private TextInputLayout nameLayout;
    private TextInputLayout positionLayout;
    private TextInputLayout birthdayLayout;
    private TextInputLayout salaryLayout;

    private Calendar birthday = null;
    private boolean editFlag = false;
    private Employee employee;

    private boolean exitFlag = false;
    private AlertDialog discardChangeDialog;
    private SQLiteDatabase database;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(0, 0, 0, R.string.button_add).setIcon(R.drawable.ic_done_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long result = 0;
        switch (item.getItemId()) {
            case 0:
                exitFlag = true;
                if (!checkValues()) {
                    return true;
                }
                if (editFlag) {
                    result = EmployeeDAO.update(database, employee);
                } else {
                    if(!EmployeeDAO.isUsedEmail(database, employee.getEmail())){
                        result = EmployeeDAO.add(database, employee);
                        emailLayout.setError(null);
                    } else {
                        emailLayout.setError(getString(R.string.error_email_used));
                        return true;
                    }
                }
                break;
            case 1:
                result = EmployeeDAO.delete(database, employee.getId());
                break;
        }
        if (result > 0) {
            getActivity().onBackPressed();
            Snackbar.make(((CommonWidget) getActivity()).getCoordinatorLayout(), getString(R.string.message_done), Snackbar.LENGTH_LONG).show();
            return true;
        } else {
            Snackbar.make(((CommonWidget) getActivity()).getCoordinatorLayout(), "error", Snackbar.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.title_employee_add);
        View view = inflater.inflate(R.layout.fragment_add_employee, null, false);
        ((CommonWidget) getActivity()).getFloatingActionButton().hide();
        database = OpenHelper.getDatabase(getContext());
        emailEdt = (EditText) view.findViewById(R.id.email_edt);
        emailLayout = (TextInputLayout) view.findViewById(R.id.email_layout);
        passwordEdt = (EditText) view.findViewById(R.id.password_edt);
        passwordLayout = (TextInputLayout) view.findViewById(R.id.password_layout);
        confPasswordEdt = (EditText) view.findViewById(R.id.conf_password_edt);
        confPasswordLayout = (TextInputLayout) view.findViewById(R.id.conf_password_layout);
        nameEdt = (EditText) view.findViewById(R.id.name_edt);
        positionEdt = (EditText) view.findViewById(R.id.position_edt);
        birthdayEdt = (EditText) view.findViewById(R.id.birthday_edt);
        addressEdt = (EditText) view.findViewById(R.id.address_edt);
        salaryEdt = (EditText) view.findViewById(R.id.salary_edt);
        nameLayout = (TextInputLayout) view.findViewById(R.id.name_layout);
        positionLayout = (TextInputLayout) view.findViewById(R.id.position_layout);
        birthdayLayout = (TextInputLayout) view.findViewById(R.id.birthday_layout);
        salaryLayout = (TextInputLayout) view.findViewById(R.id.salary_layout);
        birthdayEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (birthday == null) {
                    birthday = Calendar.getInstance();
                }
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        birthday.set(Calendar.YEAR, year);
                        birthday.set(Calendar.MONTH, monthOfYear);
                        birthday.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        birthdayEdt.setText(DateUtil.formatDate(birthday));
                    }

                };
                new DatePickerDialog(getContext(), date, birthday.get(Calendar.YEAR), birthday.get(Calendar.MONTH), birthday.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        if (getArguments() != null) {
            employee = getArguments().getParcelable(Constants.EMPLOYEE);
            if (employee != null) {
                editFlag = true;
                getActivity().setTitle(R.string.title_employee_edit);
                fillData();
            }
        }
        return view;
    }

    private boolean checkValues() {
        String emailStr = ValidationUtil.editTextValidation(emailEdt, emailLayout, getString(R.string.error_miss_email));
        if (emailStr != null) {
            if (!ValidationUtil.isValidEmail(emailStr)) {
                emailStr = null;
                emailLayout.setError(getString(R.string.error_not_valid_email));
            } else {
                emailLayout.setError(null);
            }
        }

        String passwordStr = ValidationUtil.editTextValidation(passwordEdt, passwordLayout, getString(R.string.error_miss_password));
        String confPasswordStr = ValidationUtil.editTextValidation(confPasswordEdt, confPasswordLayout, getString(R.string.error_miss_password));
        if(employee != null){
            if(passwordStr.equals(employee.getPassword())){
                confPasswordStr = passwordStr;
            }
        }

        String nameStr = ValidationUtil.editTextValidation(nameEdt, nameLayout, getString(R.string.error_miss_name));
        String positionStr = ValidationUtil.editTextValidation(positionEdt, positionLayout, getString(R.string.error_miss_position));
        String birthdayStr = ValidationUtil.editTextValidation(birthdayEdt);
        boolean haveError = false;
        if (birthdayStr != null) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                Date date = format.parse(birthdayStr);
                birthday = Calendar.getInstance();
                birthday.setTime(date);
                haveError = false;
            } catch (ParseException e) {
                birthdayLayout.setError(getString(R.string.error_not_valid_birthday));
                haveError = true;
            }
        }

        String addressStr = ValidationUtil.editTextValidation(addressEdt);
        String salaryStr = ValidationUtil.editTextValidation(salaryEdt);
        Integer salary = null;
        if (salaryStr != null) {
            try {
                salary = Integer.parseInt(salaryStr);
                haveError = false;
            } catch (NumberFormatException e) {
                haveError = true;
                salaryLayout.setError(getString(R.string.error_not_valid_salary));
            }
        }

        if (!ValidationUtil.isNoNull(emailStr, passwordStr, confPasswordStr, nameStr, positionStr) || haveError) {
            return false;
        }

        if (!passwordStr.equals(confPasswordStr)) {
            confPasswordLayout.setError(getString(R.string.error_password_no_match));
            return false;
        } else {
            confPasswordLayout.setError(null);
        }

        if (employee == null) {
            employee = new Employee();
        }

        employee.setEmail(emailStr);
        employee.setPassword(passwordStr);
        employee.setName(nameStr);
        employee.setPosition(positionStr);
        employee.setAddress(addressStr);
        employee.setBirthday(birthday);
        employee.setSalary(salary);

        return true;
    }

    private void fillData() {
        emailEdt.setText(employee.getEmail());
        passwordEdt.setText(employee.getPassword());
        nameEdt.setText(employee.getName());
        positionEdt.setText(employee.getPosition());
        addressEdt.setText(employee.getAddress());
        birthdayEdt.setText(DateUtil.formatDate(employee.getBirthday()));
        salaryEdt.setText(String.valueOf(employee.getSalary() == null ? "" : employee.getSalary()));
        birthday = employee.getBirthday();
        emailEdt.setEnabled(false);
    }

    /*private void showDiscardChangeDialog() {
        if (discardChangeDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage(R.string.message_discard_change);
            builder.setPositiveButton(R.string.button_discard, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            });
            builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            discardChangeDialog = builder.create();
        }
        if (!discardChangeDialog.isShowing()) {
            discardChangeDialog.show();
        }
    }*/
}
