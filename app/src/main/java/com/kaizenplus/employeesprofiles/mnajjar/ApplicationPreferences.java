package com.kaizenplus.employeesprofiles.mnajjar;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class ApplicationPreferences {

    private SharedPreferences preferences;

    private static ApplicationPreferences instance;

    public static ApplicationPreferences getPreferences(Context context) {
        if (instance == null) {
            instance = new ApplicationPreferences(context);
        }
        return instance;
    }

    private ApplicationPreferences(Context context) {
        preferences = context.getSharedPreferences("com.kaizenplus.employeesprofiles.mnajjar", Context.MODE_PRIVATE);
    }

    public void putString(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }
}
