package com.kaizenplus.employeesprofiles.mnajjar.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.activity.ChangeFragment;
import com.kaizenplus.employeesprofiles.mnajjar.activity.CommonWidget;
import com.kaizenplus.employeesprofiles.mnajjar.adapter.RecyclerViewAdapter;
import com.kaizenplus.employeesprofiles.mnajjar.database.EmployeeDAO;
import com.kaizenplus.employeesprofiles.mnajjar.database.OpenHelper;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Detail;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;
import com.kaizenplus.employeesprofiles.mnajjar.util.DateUtil;
import com.kaizenplus.employeesprofiles.mnajjar.viewholder.DetailViewHolder;

import java.util.Locale;

/**
 * date 7/15/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class EmployeeDetail extends Fragment {

    private Employee employee;
    private AlertDialog deleteDialog;

    private RecyclerViewAdapter<Detail, DetailViewHolder> adapter = new RecyclerViewAdapter<Detail, DetailViewHolder>() {
        @Override
        public DetailViewHolder cViewHolder(ViewGroup viewGroup, int i, LayoutInflater layoutInflater) {
            return new DetailViewHolder(layoutInflater.inflate(R.layout.item_detail, viewGroup, false));
        }

        @Override
        public void bViewHolder(DetailViewHolder viewHolder, int i, Detail item) {
            viewHolder.getText1().setText(item.getValue());
            viewHolder.getText2().setText(item.getDesc());
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(ApplicationPreferences.getPreferences(getContext()).getBoolean(Constants.IS_ADMIN, false)){
            if (!employee.getAdmin()) {
                menu.add(0, 1, 0, R.string.button_delete).setIcon(R.drawable.ic_delete_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }
            menu.add(0, 0, 0, R.string.button_edit).setIcon(R.drawable.ic_create_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.EMPLOYEE, employee);
                EmployeeEdit employeeEdit = new EmployeeEdit();
                employeeEdit.setArguments(bundle);
                ((ChangeFragment) getActivity()).setFragment(employeeEdit, false);
                return true;
            case 1:
                showConfirmDeleteDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((CommonWidget) getActivity()).getFloatingActionButton().hide();
        if (getArguments() == null) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        employee = getArguments().getParcelable(Constants.EMPLOYEE);
        if (employee == null) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        getActivity().setTitle(employee.getName());
        View view = inflater.inflate(R.layout.fragment_list, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.clear();
        adapter.add(
                new Detail(employee.getName(), getString(R.string.hint_name)),
                new Detail(employee.getPosition(), getString(R.string.hint_position)),
                new Detail(employee.getEmail(), getString(R.string.hint_email)));
        if (employee.getAddress() != null) {
            adapter.add(new Detail(employee.getAddress(), getString(R.string.hint_address)));
        }
        if (employee.getBirthday() != null) {
            adapter.add(new Detail(DateUtil.formatDate(employee.getBirthday()), getString(R.string.hint_birthday)));
        }
        if (employee.getSalary() != null && employee.getSalary() != 0) {
            adapter.add(new Detail(String.valueOf(employee.getSalary()), getString(R.string.hint_salary)));
        }
        return view;
    }

    private void showConfirmDeleteDialog() {
        if (deleteDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage(String.format(Locale.US, getString(R.string.message_delete_employee), employee.getName()));
            builder.setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (EmployeeDAO.delete(OpenHelper.getDatabase(getContext()), employee.getId()) > 0) {
                        getActivity().onBackPressed();
                        Snackbar.make(((CommonWidget) getActivity()).getCoordinatorLayout(), getString(R.string.message_done), Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(((CommonWidget) getActivity()).getCoordinatorLayout(), "error", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            deleteDialog = builder.create();
        }
        if(!deleteDialog.isShowing()){
            deleteDialog.show();
        }
    }
}
