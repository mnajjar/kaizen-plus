package com.kaizenplus.employeesprofiles.mnajjar.objects;

/**
 * date 7/15/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class Detail {

    private String desc;
    private String value;

    public Detail(String value, String desc) {
        this.desc = desc;
        this.value = value;
    }

    public Detail() {
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
