package com.kaizenplus.employeesprofiles.mnajjar;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public interface Constants {

    String IS_ADMIN = "isAdmin";
    String LOG_EMAIL = "logEmail";
    String EMAIL = "email";
    String EMPLOYEE = "employee";
    String FIRST_TIME = "firstTime";
    String FIRST_INFO = "firstInfo";


}
