package com.kaizenplus.employeesprofiles.mnajjar.util;

import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class ValidationUtil {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static String editTextValidation(EditText editText, TextInputLayout layout, String error) {
        String str = editText.getText().toString().trim();
        if (str.isEmpty()) {
            str = null;
            layout.setError(error);
        } else {
            layout.setError(null);
        }
        return str;
    }

    public static String editTextValidation(EditText editText){
        String str = editText.getText().toString().trim();
        return str.isEmpty() ? null : str;
    }

    public static boolean isValidEmail(String emailStr) {
        if (emailStr == null) {
            return false;
        }
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean isNoNull(Object... values) {
        for (Object o : values) {
            if (o == null) {
                return false;
            }
        }
        return true;
    }

}
