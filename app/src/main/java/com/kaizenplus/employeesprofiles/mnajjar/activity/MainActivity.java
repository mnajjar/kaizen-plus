package com.kaizenplus.employeesprofiles.mnajjar.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.fragment.AddAdmin;
import com.kaizenplus.employeesprofiles.mnajjar.fragment.AddAdminInfo;
import com.kaizenplus.employeesprofiles.mnajjar.fragment.Home;
import com.kaizenplus.employeesprofiles.mnajjar.fragment.Login;

/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class MainActivity extends AppCompatActivity implements ChangeFragment, CommonWidget {

    private FloatingActionButton floatingBtn;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        floatingBtn = (FloatingActionButton) findViewById(R.id.fab);
        floatingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).show();
            }
        });

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.rootview);

        initFragment();
    }

    @Override
    public void setFragment(Fragment fragment, boolean clearBackStack) {
        if (clearBackStack) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        }
        System.out.println("CurrentFragment: " + fragment.getClass().getSimpleName());
        System.out.println("BackStackEntryCount: " + getSupportFragmentManager().getBackStackEntryCount());
    }

    @Override
    public FloatingActionButton getFloatingActionButton() {
        return floatingBtn;
    }

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    private void initFragment() {
        if (!ApplicationPreferences.getPreferences(this).getBoolean(Constants.FIRST_TIME, false)) {
            setFragment(new AddAdmin(), false);
            return;
        }

        if (!ApplicationPreferences.getPreferences(this).getBoolean(Constants.FIRST_INFO, false)) {
            setFragment(new AddAdminInfo(), false);
            return;
        }

        String logEmail = ApplicationPreferences.getPreferences(this).getString(Constants.LOG_EMAIL, null);
        if (logEmail == null) {
            setFragment(new Login(), false);
            return;
        }

        setFragment(new Home(), false);
    }
}
