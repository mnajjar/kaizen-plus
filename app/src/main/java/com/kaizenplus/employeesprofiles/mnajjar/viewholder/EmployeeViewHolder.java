package com.kaizenplus.employeesprofiles.mnajjar.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kaizenplus.employeesprofiles.R;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class EmployeeViewHolder extends RecyclerView.ViewHolder {

    private TextView text1;
    private TextView text2;

    public EmployeeViewHolder(View itemView) {
        super(itemView);
        text1 = (TextView) itemView.findViewById(R.id.text1);
        text2 = (TextView) itemView.findViewById(R.id.text2);
    }

    public TextView getText1() {
        return text1;
    }

    public TextView getText2() {
        return text2;
    }
}
