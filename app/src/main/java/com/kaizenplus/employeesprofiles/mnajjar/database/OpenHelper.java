package com.kaizenplus.employeesprofiles.mnajjar.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class OpenHelper extends SQLiteOpenHelper {

    private static SQLiteDatabase database;

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null) {
            database = new OpenHelper(context).getWritableDatabase();
        }
        return database;
    }

    private static final String NAME = "database.db";
    private static final int VERSION = 1;

    public OpenHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE EMPLOYEE ( " +
                Contract.Employee.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                Contract.Employee.NAME + " TEXT    NOT NULL, " +
                Contract.Employee.POSITION + " TEXT    NOT NULL, " +
                Contract.Employee.BIRTHDAY + " INTEGER, " +
                Contract.Employee.EMAIL + " TEXT NOT NULL UNIQUE, " +
                Contract.Employee.PASSWORD + " TEXT NOT NULL, " +
                Contract.Employee.ADDRESS + " TEXT, " +
                Contract.Employee.SALARY + " INTEGER, " +
                Contract.Employee.IS_ADMIN + " INTEGER NOT NULL )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
