package com.kaizenplus.employeesprofiles.mnajjar.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class DateUtil {

    public static final String PATTERN_DD_MMM_YYYY = "dd MMM yyyy";
    private static SimpleDateFormat format = new SimpleDateFormat(PATTERN_DD_MMM_YYYY, Locale.US);

    public static String formatDate(Calendar calendar, String pattern) {
        if (calendar == null || pattern == null) {
            return "";
        }
        format.applyPattern(pattern);
        return format.format(calendar.getTime());
    }

    public static String formatDate(Calendar calendar) {
        return formatDate(calendar, PATTERN_DD_MMM_YYYY);
    }

}
