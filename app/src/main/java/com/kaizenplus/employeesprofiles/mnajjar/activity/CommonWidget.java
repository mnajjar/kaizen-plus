package com.kaizenplus.employeesprofiles.mnajjar.activity;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public interface CommonWidget {

    FloatingActionButton getFloatingActionButton();
    CoordinatorLayout getCoordinatorLayout();

}
