package com.kaizenplus.employeesprofiles.mnajjar.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.activity.ChangeFragment;
import com.kaizenplus.employeesprofiles.mnajjar.activity.CommonWidget;
import com.kaizenplus.employeesprofiles.mnajjar.database.EmployeeDAO;
import com.kaizenplus.employeesprofiles.mnajjar.database.OpenHelper;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;
import com.kaizenplus.employeesprofiles.mnajjar.util.ValidationUtil;

/**
 * date 7/15/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class Login extends Fragment {

    private EditText emailEdt;
    private EditText passwordEdit;
    private TextInputLayout emailLayout;
    private TextInputLayout passwordLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((CommonWidget) getActivity()).getFloatingActionButton().hide();
        getActivity().setTitle(R.string.title_login);
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        emailEdt = (EditText) view.findViewById(R.id.email_edt);
        emailLayout = (TextInputLayout) view.findViewById(R.id.email_layout);
        passwordEdit = (EditText) view.findViewById(R.id.password_edt);
        passwordLayout = (TextInputLayout) view.findViewById(R.id.password_layout);

        view.findViewById(R.id.login_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailStr = ValidationUtil.editTextValidation(emailEdt, emailLayout, getString(R.string.error_miss_email));
                String passwordStr = ValidationUtil.editTextValidation(passwordEdit, passwordLayout, getString(R.string.error_miss_password));

                if (!ValidationUtil.isNoNull(emailStr, passwordStr)) {
                    return;
                }

                if (!ValidationUtil.isValidEmail(emailStr)) {
                    emailLayout.setError(getString(R.string.error_not_valid_email));
                    return;
                } else {
                    emailLayout.setError(null);
                }

                Employee employee = EmployeeDAO.getByEmail(OpenHelper.getDatabase(getContext()), emailStr);
                if (employee == null) {
                    emailLayout.setError(getString(R.string.error_wrong_email));
                    return;
                } else {
                    emailLayout.setError(null);
                }

                if(!employee.getPassword().equals(passwordStr)){
                    passwordLayout.setError(getString(R.string.error_wrong_password));
                    return;
                } else {
                    passwordLayout.setError(null);
                }

                ApplicationPreferences.getPreferences(getContext()).putString(Constants.LOG_EMAIL, employee.getEmail());
                ApplicationPreferences.getPreferences(getContext()).putBoolean(Constants.IS_ADMIN, employee.getAdmin());
                ((ChangeFragment) getActivity()).setFragment(new Home(), true);
            }
        });
        return view;
    }
}
