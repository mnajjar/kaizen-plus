package com.kaizenplus.employeesprofiles.mnajjar.fragment;

import android.app.DatePickerDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.activity.ChangeFragment;
import com.kaizenplus.employeesprofiles.mnajjar.database.EmployeeDAO;
import com.kaizenplus.employeesprofiles.mnajjar.database.OpenHelper;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;
import com.kaizenplus.employeesprofiles.mnajjar.util.DateUtil;
import com.kaizenplus.employeesprofiles.mnajjar.util.ValidationUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class AddAdminInfo extends Fragment {

    private EditText nameEdt;
    private EditText positionEdt;
    private EditText birthdayEdt;
    private EditText addressEdt;
    private EditText salaryEdt;
    private TextInputLayout nameLayout;
    private TextInputLayout positionLayout;
    private TextInputLayout birthdayLayout;
    private TextInputLayout salaryLayout;

    private String emailStr;
    private Calendar birthday = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        getActivity().setTitle(R.string.title_admin_info);
        emailStr = ApplicationPreferences.getPreferences(getContext()).getString(Constants.EMAIL, null);
        if (emailStr == null) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }

        View view = inflater.inflate(R.layout.fragment_add_admin_info, null);
        nameEdt = (EditText) view.findViewById(R.id.name_edt);
        positionEdt = (EditText) view.findViewById(R.id.position_edt);
        birthdayEdt = (EditText) view.findViewById(R.id.birthday_edt);
        addressEdt = (EditText) view.findViewById(R.id.address_edt);
        salaryEdt = (EditText) view.findViewById(R.id.salary_edt);
        nameLayout = (TextInputLayout) view.findViewById(R.id.name_layout);
        positionLayout = (TextInputLayout) view.findViewById(R.id.position_layout);
        birthdayLayout = (TextInputLayout) view.findViewById(R.id.birthday_layout);
        salaryLayout = (TextInputLayout) view.findViewById(R.id.salary_layout);


        birthdayEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (birthday == null) {
                    birthday = Calendar.getInstance();
                }
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        birthday.set(Calendar.YEAR, year);
                        birthday.set(Calendar.MONTH, monthOfYear);
                        birthday.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        birthdayEdt.setText(DateUtil.formatDate(birthday));
                    }

                };
                new DatePickerDialog(getContext(), date, birthday.get(Calendar.YEAR), birthday.get(Calendar.MONTH), birthday.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        view.findViewById(R.id.finish_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameStr = ValidationUtil.editTextValidation(nameEdt, nameLayout, getString(R.string.error_miss_name));
                String positionStr = ValidationUtil.editTextValidation(positionEdt, positionLayout, getString(R.string.error_miss_position));
                String birthdayStr = ValidationUtil.editTextValidation(birthdayEdt);
                boolean haveError = false;
                if (birthdayStr != null) {
                    try {
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                        Date date = format.parse(birthdayStr);
                        birthday = Calendar.getInstance();
                        birthday.setTime(date);
                        haveError = false;
                    } catch (ParseException e) {
                        birthdayLayout.setError(getString(R.string.error_not_valid_birthday));
                        haveError = true;
                    }
                }

                String addressStr = ValidationUtil.editTextValidation(addressEdt);
                String salaryStr = ValidationUtil.editTextValidation(salaryEdt);
                Integer salary = null;
                if (salaryStr != null) {
                    try {
                        salary = Integer.parseInt(salaryStr);
                        haveError = false;
                    } catch (NumberFormatException e) {
                        haveError = true;
                        salaryLayout.setError(getString(R.string.error_not_valid_salary));
                    }
                }

                if (!ValidationUtil.isNoNull(nameStr, positionStr) || haveError) {
                    return;
                }

                SQLiteDatabase database = OpenHelper.getDatabase(getContext());

                Employee employee = EmployeeDAO.getByEmail(database, emailStr);
                employee.setName(nameStr);
                employee.setPosition(positionStr);
                employee.setBirthday(birthday);
                employee.setAddress(addressStr);
                employee.setSalary(salary);
                if (EmployeeDAO.update(database, employee) > 0) {
                    ApplicationPreferences.getPreferences(getContext()).putBoolean(Constants.FIRST_INFO, true);
                    ((ChangeFragment) getActivity()).setFragment(new Home(), true);
                } else {
                    Snackbar.make(v, "error", Snackbar.LENGTH_LONG).show();
                }

            }
        });

        return view;
    }


}
