package com.kaizenplus.employeesprofiles.mnajjar.database;

/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class Contract {

    public interface Employee {
        String TABLE_NAME = "EMPLOYEE";
        String ID = "ID";
        String NAME = "NAME";
        String POSITION = "POSITION";
        String BIRTHDAY = "BIRTHDAY";
        String EMAIL = "EMAIL";
        String ADDRESS = "ADDRESS";
        String SALARY = "SALARY";
        String IS_ADMIN = "IS_ADMIN";
        String PASSWORD = "PASSWORD";
    }

}
