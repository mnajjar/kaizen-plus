package com.kaizenplus.employeesprofiles.mnajjar.activity;

import android.support.v4.app.Fragment;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public interface ChangeFragment {

    void setFragment(Fragment fragment, boolean clearBackStack);

}
