package com.kaizenplus.employeesprofiles.mnajjar.objects;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.kaizenplus.employeesprofiles.mnajjar.database.Contract;
import com.kaizenplus.employeesprofiles.mnajjar.util.CursorUtil;

import java.util.Calendar;

/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class Employee implements Parcelable {


    private Integer id;
    private String name;
    private String position;
    private Calendar birthday;
    private String email;
    private String password;
    private String address;
    private Integer salary;
    private Boolean isAdmin;

    public Employee(@NonNull Cursor cursor) {
        this.id = CursorUtil.getColumnValue(cursor, Contract.Employee.ID, Integer.class);
        this.name = CursorUtil.getColumnValue(cursor, Contract.Employee.NAME, String.class);
        this.position = CursorUtil.getColumnValue(cursor, Contract.Employee.POSITION, String.class);
        Long birthdayInMillis = CursorUtil.getColumnValue(cursor, Contract.Employee.BIRTHDAY, Long.class);
        if(birthdayInMillis != null){
            this.birthday = Calendar.getInstance();
            this.birthday.setTimeInMillis(birthdayInMillis);
        }
        this.email = CursorUtil.getColumnValue(cursor, Contract.Employee.EMAIL, String.class);
        this.password = CursorUtil.getColumnValue(cursor, Contract.Employee.PASSWORD, String.class);
        this.address = CursorUtil.getColumnValue(cursor, Contract.Employee.ADDRESS, String.class);
        this.salary = CursorUtil.getColumnValue(cursor, Contract.Employee.SALARY, Integer.class);
        Integer isAdmin = CursorUtil.getColumnValue(cursor, Contract.Employee.IS_ADMIN, Integer.class);
        this.isAdmin = isAdmin != null ? isAdmin == 1 : null;
    }

    public Employee() {
        this.isAdmin = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        CursorUtil.putObject(values, Contract.Employee.NAME, getName());
        CursorUtil.putObject(values, Contract.Employee.POSITION, getPosition());
        CursorUtil.putObject(values, Contract.Employee.BIRTHDAY, getBirthday());
        CursorUtil.putObject(values, Contract.Employee.EMAIL, getEmail().toLowerCase());
        CursorUtil.putObject(values, Contract.Employee.ADDRESS, getAddress());
        CursorUtil.putObject(values, Contract.Employee.SALARY, getSalary());
        CursorUtil.putObject(values, Contract.Employee.IS_ADMIN, getAdmin());
        CursorUtil.putObject(values, Contract.Employee.PASSWORD, getPassword());
        return values;
    }

    @Override
    public String toString() {
        return "{" + "\"name\":\"" + name + "\"" +
                ",\"position\":\"" + position + "\"" +
                ",\"birthday\":" + (birthday != null ? birthday.getTimeInMillis() : "null") +
                ",\"email\":\"" + email + "\"" +
                ",\"address\":\"" + address + "\"" +
                ",\"salary\":" + salary +
                ",\"isAdmin\":" + isAdmin +
                "}";
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.position);
        dest.writeSerializable(this.birthday);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.address);
        dest.writeValue(this.salary);
        dest.writeValue(this.isAdmin);
    }

    public Employee(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.position = in.readString();
        this.birthday = (Calendar) in.readSerializable();
        this.email = in.readString();
        this.password = in.readString();
        this.address = in.readString();
        this.salary = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Employee> CREATOR = new Parcelable.Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}
