package com.kaizenplus.employeesprofiles.mnajjar.util;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.Calendar;

/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class CursorUtil {

    @SuppressWarnings("unchecked")
    public static <T> T getColumnValue(Cursor cursor, String columnName, Class c) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1 || cursor.isNull(index)) {
            return null;
        }
        if (c == String.class) {
            return (T) cursor.getString(index);
        }
        if (c == Integer.class) {
            return (T) Integer.valueOf(cursor.getInt(index));
        }
        if (c == Double.class) {
            return (T) Double.valueOf(cursor.getDouble(index));
        }
        if (c == Long.class) {
            return (T) Long.valueOf(cursor.getLong(index));
        }
        return null;
    }

    public static void putObject(ContentValues values, String key, Object o) {
        if (o == null) {
            values.putNull(key);
            return;
        }
        if (o instanceof String) {
            values.put(key, (String) o);
        } else if (o instanceof Integer) {
            values.put(key, (Integer) o);
        } else if (o instanceof Long) {
            values.put(key, (Long) o);
        } else if (o instanceof Boolean) {
            values.put(key, ((Boolean) o) ? 1 : 0);
        } else if (o instanceof Calendar) {
            values.put(key, ((Calendar) o).getTimeInMillis());
        } else {
            values.putNull(key);
        }
    }

}
