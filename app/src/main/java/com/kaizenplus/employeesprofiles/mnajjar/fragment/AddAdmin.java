package com.kaizenplus.employeesprofiles.mnajjar.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.activity.ChangeFragment;
import com.kaizenplus.employeesprofiles.mnajjar.database.EmployeeDAO;
import com.kaizenplus.employeesprofiles.mnajjar.database.OpenHelper;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;
import com.kaizenplus.employeesprofiles.mnajjar.util.ValidationUtil;

import java.util.Calendar;

/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class AddAdmin extends Fragment {

    private EditText emailEdt;
    private EditText passwordEdt;
    private EditText confPasswordEdt;
    private TextInputLayout emailLayout;
    private TextInputLayout passwordLayout;
    private TextInputLayout confPasswordLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.title_admin_add);
        View view = inflater.inflate(R.layout.fragment_add_admin, null, false);
        emailEdt = (EditText) view.findViewById(R.id.email_edt);
        emailLayout = (TextInputLayout) view.findViewById(R.id.email_layout);
        passwordEdt = (EditText) view.findViewById(R.id.password_edt);
        passwordLayout = (TextInputLayout) view.findViewById(R.id.password_layout);
        confPasswordEdt = (EditText) view.findViewById(R.id.conf_password_edt);
        confPasswordLayout = (TextInputLayout) view.findViewById(R.id.conf_password_layout);

        view.findViewById(R.id.next_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailStr = ValidationUtil.editTextValidation(emailEdt, emailLayout, getString(R.string.error_miss_email));
                if (emailStr != null) {
                    if (!ValidationUtil.isValidEmail(emailStr)) {
                        emailStr = null;
                        emailLayout.setError(getString(R.string.error_not_valid_email));
                    } else {
                        emailLayout.setError(null);
                    }
                }

                String passwordStr = ValidationUtil.editTextValidation(passwordEdt, passwordLayout, getString(R.string.error_miss_password));
                String confPasswordStr = ValidationUtil.editTextValidation(confPasswordEdt, confPasswordLayout, getString(R.string.error_miss_password));
                if (!ValidationUtil.isNoNull(emailStr, passwordStr, confPasswordStr)) {
                    return;
                }

                if (!passwordStr.equals(confPasswordStr)) {
                    confPasswordLayout.setError(getString(R.string.error_password_no_match));
                    return;
                } else {
                    confPasswordLayout.setError(null);
                }

                Employee employee = new Employee();
                employee.setName("Admin");
                employee.setEmail(emailStr);
                employee.setPassword(passwordStr);
                employee.setAdmin(true);
                employee.setPosition("admin");
                employee.setSalary(0);
                employee.setBirthday(Calendar.getInstance());

                if (EmployeeDAO.add(OpenHelper.getDatabase(getContext()), employee) > 0) {
                    ApplicationPreferences.getPreferences(getContext()).putString(Constants.LOG_EMAIL, emailStr);
                    ApplicationPreferences.getPreferences(getContext()).putBoolean(Constants.IS_ADMIN, true);
                    ((ChangeFragment) getActivity()).setFragment(new AddAdminInfo(), true);
                    ApplicationPreferences.getPreferences(getContext()).putBoolean(Constants.FIRST_TIME, true);
                    ApplicationPreferences.getPreferences(getContext()).putString(Constants.EMAIL, emailStr);
                } else {
                    Snackbar.make(v, "error", Snackbar.LENGTH_LONG).show();
                }

            }
        });

        return view;

    }
}
