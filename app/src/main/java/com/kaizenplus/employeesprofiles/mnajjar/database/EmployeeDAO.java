package com.kaizenplus.employeesprofiles.mnajjar.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;


/**
 * date: 7/13/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class EmployeeDAO {

    public static long add(@NonNull SQLiteDatabase database, @NonNull Employee employee) {
        database.beginTransaction();
        long result = database.insert(Contract.Employee.TABLE_NAME, null, employee.getContentValues());
        database.setTransactionSuccessful();
        database.endTransaction();
        return result;
    }

    public static long update(@NonNull SQLiteDatabase database, @NonNull Employee employee) {
        database.beginTransaction();
        int rowsAffected = database.update(Contract.Employee.TABLE_NAME, employee.getContentValues(), Contract.Employee.ID + " = ?", new String[]{String.valueOf(employee.getId())});
        database.setTransactionSuccessful();
        database.endTransaction();
        return rowsAffected;
    }

    public static long delete(@NonNull SQLiteDatabase database, int id) {
        database.beginTransaction();
        int rowsAffected = database.delete(Contract.Employee.TABLE_NAME, Contract.Employee.ID + " = ?", new String[]{String.valueOf(id)});
        database.setTransactionSuccessful();
        database.endTransaction();
        return rowsAffected;
    }

    public static Cursor getAll(@NonNull SQLiteDatabase database) {
        return database.rawQuery("SELECT * FROM " + Contract.Employee.TABLE_NAME + " ORDER BY LOWER(" + Contract.Employee.NAME + ")", null);
    }

    public static Employee getByEmail(@NonNull SQLiteDatabase database, @NonNull String email) {
        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + Contract.Employee.TABLE_NAME +
                        " WHERE LOWER(" + Contract.Employee.EMAIL + ") = LOWER('" + email + "') ORDER BY LOWER(" + Contract.Employee.NAME + ")", null
        );
        Employee employee = null;
        if (cursor.moveToNext()) {
            employee = new Employee(cursor);
        }
        cursor.close();
        return employee;
    }

    public static Cursor searchByName(@NonNull SQLiteDatabase database, String str) {
        return database.rawQuery(
                "SELECT * FROM " + Contract.Employee.TABLE_NAME +
                        " WHERE LOWER(" + Contract.Employee.NAME + ") LIKE LOWER('%" + str + "%')", null);
    }

    public static boolean isUsedEmail(@NonNull SQLiteDatabase database, String email) {
        Cursor cursor = database.rawQuery(
                "SELECT 1 FROM " + Contract.Employee.TABLE_NAME +
                        " WHERE LOWER(" + Contract.Employee.EMAIL + ") = LOWER('" + email + "')",
                null
        );
        boolean result = cursor.moveToNext();
        cursor.close();
        return result;
    }
}
