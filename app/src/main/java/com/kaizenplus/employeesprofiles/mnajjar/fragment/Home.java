package com.kaizenplus.employeesprofiles.mnajjar.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.kaizenplus.employeesprofiles.R;
import com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences;
import com.kaizenplus.employeesprofiles.mnajjar.Constants;
import com.kaizenplus.employeesprofiles.mnajjar.activity.ChangeFragment;
import com.kaizenplus.employeesprofiles.mnajjar.activity.CommonWidget;
import com.kaizenplus.employeesprofiles.mnajjar.adapter.RecyclerViewAdapter;
import com.kaizenplus.employeesprofiles.mnajjar.database.EmployeeDAO;
import com.kaizenplus.employeesprofiles.mnajjar.database.OpenHelper;
import com.kaizenplus.employeesprofiles.mnajjar.objects.Employee;
import com.kaizenplus.employeesprofiles.mnajjar.viewholder.EmployeeViewHolder;

import static com.kaizenplus.employeesprofiles.mnajjar.ApplicationPreferences.getPreferences;

/**
 * date: 7/14/2016
 *
 * @author Mohammad Al-Najjar
 * @since 1.0
 */
public class Home extends Fragment {

    private AsyncTask<String, Employee, Void> employeeAsync;
    private RecyclerViewAdapter<Employee, EmployeeViewHolder> adapter = new RecyclerViewAdapter<Employee, EmployeeViewHolder>() {
        @Override
        public EmployeeViewHolder cViewHolder(ViewGroup viewGroup, int i, LayoutInflater layoutInflater) {
            return new EmployeeViewHolder(layoutInflater.inflate(R.layout.item_employee, null, false));
        }

        @Override
        public void bViewHolder(EmployeeViewHolder viewHolder, int i, final Employee item) {
            viewHolder.getText1().setText(item.getName());
            viewHolder.getText2().setText(item.getPosition());
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEmployeeDetail(item);
                }
            });
        }
    };

    private void showEmployeeDetail(Employee item) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.EMPLOYEE, item);
        EmployeeDetail employeeDetail = new EmployeeDetail();
        employeeDetail.setArguments(bundle);
        ((ChangeFragment) getActivity()).setFragment(employeeDetail, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.title_home);
        boolean isAdmin = ApplicationPreferences.getPreferences(getContext()).getBoolean(Constants.IS_ADMIN, false);
        if(isAdmin){
            FloatingActionButton floatingButton = ((CommonWidget) getActivity()).getFloatingActionButton();
            floatingButton.show();
            floatingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ChangeFragment) getActivity()).setFragment(new EmployeeEdit(), false);
                }
            });
        }
        View view = inflater.inflate(R.layout.fragment_list, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        employeeAsync = initEmployeeList();
        employeeAsync.execute();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.home_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    employeeAsync.cancel(true);
                    employeeAsync = initEmployeeList();
                    if (newText == null || newText.isEmpty()) {
                        employeeAsync.execute();
                    } else {
                        employeeAsync.execute(newText);
                    }
                    return true;
                }
            });
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_logout:
                getPreferences(getContext()).putString(Constants.LOG_EMAIL, null);
                ((ChangeFragment) getActivity()).setFragment(new Login(), true);
                return true;
            case R.id.item_profile:
                String logEmail = ApplicationPreferences.getPreferences(getContext()).getString(Constants.LOG_EMAIL, null);
                showEmployeeDetail(EmployeeDAO.getByEmail(OpenHelper.getDatabase(getContext()), logEmail));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private AsyncTask<String, Employee, Void> initEmployeeList() {
        return new AsyncTask<String, Employee, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                adapter.clear();
            }

            @Override
            protected Void doInBackground(String... params) {
                Cursor cursor;
                if (params.length > 0) {
                    cursor = EmployeeDAO.searchByName(OpenHelper.getDatabase(getContext()), params[0]);
                } else {
                    cursor = EmployeeDAO.getAll(OpenHelper.getDatabase(getContext()));
                }

                while (cursor.moveToNext()) {
                    publishProgress(new Employee(cursor));
                }
                cursor.close();
                return null;
            }

            @Override
            protected void onProgressUpdate(Employee... values) {
                adapter.add(values[0]);
            }
        };
    }
}
